module Animation where

import Shape
import Data.Fixed
import Draw
import Picture
import CustomSOE hiding (Region)
import qualified CustomSOE as G (Region)

--Custom data type for my use
data UpdatableImage = 
  StaticImage {getPic :: Picture} | 
  DynamicImage {getAnim :: (Animation Picture)}

type Animation a = Time -> a
type Time = Float

rubberBall :: Animation Shape
rubberBall t = Ellipse (sin t)(cos t)

revolvingBall :: Animation Region
revolvingBall t
    = let ball = Shape (Ellipse 0.2 0.2)
      in Translate (sin t, cos t) ball

planets :: Animation Picture
planets t
    = let p1 = Region Red (Shape(rubberBall t))
          p2 = Region Yellow (revolvingBall t)
      in p1 `Over` p2

tellTime :: Animation String
tellTime t = "The time is: " ++ show t

animate :: String -> Animation Graphic -> IO ()

animate title anim
    = runGraphics (
      do w <- openWindowEx title (Just (0,0))(Just (xWin, yWin)) drawBufferedGraphic (Just 30)
         t0 <- timeGetTime
         let loop =
                do  t <- timeGetTime
                    let ft = intToFloat (word32ToInt (t - t0))/1000
                    setGraphic w (anim ft)
                    getWindowTick w
                    loop
         loop
      )

regionToGraphic :: Region -> Graphic
regionToGraphic = drawRegion . regionToGRegion

test1 :: Animation Picture
test1 t = (Region (if ((t `mod'` 2) > 1) then Red else Blue) (Shape (Rectangle 1 (sin t))))

test2 :: Animation Picture
test2 t = Region Green (Translate (0, (sin t)) $ Shape (Ellipse 1 1))
           
main1 :: IO ()
main1 = animate "Animated Shape" (withColor Blue . shapeToGraphic . rubberBall)

main2 :: IO ()
main2 = animate "Animated Text" (text (100,200) . tellTime)

main3 :: IO ()
main3 = animate "Animated Region" (withColor Yellow . regionToGraphic . revolvingBall)

picToGraphic :: Picture -> Graphic
picToGraphic (Region c r) = withColor c (regionToGraphic r)
picToGraphic (p1 `Over` p2) = picToGraphic p1 `overGraphic` picToGraphic p2
picToGraphic EmptyPic = emptyGraphic

picToTransparentGraphic :: Double -> Picture -> Graphic
picToTransparentGraphic alpha (Region c r) = withAlphaColor c alpha (regionToGraphic r)
picToTransparentGraphic alpha (p1 `Over` p2) = (picToTransparentGraphic alpha p1) `overGraphic` (picToTransparentGraphic alpha p2)
picToTransparentGraphic _ EmptyPic = emptyGraphic

main4 :: IO ()
main4 = animate "Animated Picture" (picToGraphic . planets)

emptyA :: Animation Picture
emptyA t = EmptyPic

overA :: Animation Picture -> Animation Picture -> Animation Picture
overA a1 a2 t = a1 t `Over` a2 t

overManyA :: [Animation Picture] -> Animation Picture
overManyA = foldr overA emptyA

newtype Behaviour a = Beh (Time -> a)

animateB :: String -> Behaviour Picture -> IO ()
animateB s (Beh pf) = animate s (picToGraphic . pf)

instance Eq (Behaviour a) where
    a1 == a2 = error "Can't compare behaviours."

instance Show (Behaviour a) where
    showsPrec n a1 = error "<< Behaviour >>"

instance Num a => Num (Behaviour a) where
    (+) = lift2 (+)
    (*) = lift2 (*)
    negate = lift1 negate
    abs = lift1 abs
    signum = lift1 signum
    fromInteger = lift0 . fromInteger

instance Fractional a => Fractional (Behaviour a) where
    (/) = lift2 (/)
    fromRational = lift0 . fromRational

instance Floating a => Floating (Behaviour a) where
    pi = lift0 pi
    sqrt = lift1 sqrt
    exp = lift1 exp
    log = lift1 log
    sin = lift1 sin
    cos = lift1 cos
    tan = lift1 tan
    asin = lift1 asin
    acos = lift1 acos
    atan = lift1 atan
    sinh = lift1 sinh
    cosh = lift1 cosh
    tanh = lift1 tanh
    asinh = lift1 asinh
    acosh = lift1 acosh
    atanh = lift1 atanh

lift0 :: a -> Behaviour a
lift0 x = Beh (\t -> x);

lift1 :: (a -> b) -> (Behaviour a -> Behaviour b)
lift1 f (Beh a) = Beh (\t -> f (a t))

lift2 :: (a -> b -> c) -> (Behaviour a -> Behaviour b -> Behaviour c)
lift2 g (Beh a) (Beh b) = Beh (\t -> g (a t) (b t))

lift3 :: (a -> b -> c -> d) -> (Behaviour a -> Behaviour b -> Behaviour c -> Behaviour d)
lift3 g (Beh a) (Beh b) (Beh c) = Beh (\t -> g (a t) (b t) (c t))

time :: Behaviour Time
time = Beh (\t -> t)

class Combine a where
    empty :: a
    over :: a -> a -> a

instance Combine [a] where
    empty = []
    over = (++)

instance Combine (Fun a) where
    empty = Fun id
    Fun a `over ` Fun b = Fun(a . b)

newtype Fun a = Fun (a -> a)

instance Combine Picture where
    empty = EmptyPic
    over = Over

instance Combine a => Combine (Behaviour a) where
    empty = lift0 empty
    over = lift2 over

overMany :: Combine a => [a] -> a
overMany = foldr over empty

reg = lift2 Region
shape = lift1 Shape
ell = lift2 Ellipse
red = lift0 Red
yellow = lift0 Yellow
translate (Beh a1, Beh a2) (Beh r) = Beh (\t -> Translate (a1 t, a2 t) (r t))

revolvingBallB :: Behaviour Picture
revolvingBallB
    = let ball = shape (ell 0.2 0.2)
      in reg flash (translate (sin time, cos time) ball)

main5 :: IO ()
main5 = animateB "Revolving Ball Behaviour" revolvingBallB

(>*) :: Ord a => Behaviour a -> Behaviour a -> Behaviour Bool
(>*) = lift2 (>)

ifFun :: Bool -> a -> a -> a
ifFun p c a = if p then c else a

cond :: Behaviour Bool -> Behaviour a -> Behaviour a -> Behaviour a
cond = lift3 ifFun

flash :: Behaviour Color
flash = cond (sin time >* 0) red yellow

timeTrans :: Behaviour Time -> Behaviour a -> Behaviour a
timeTrans (Beh f) (Beh a) = Beh (a . f)

flashingBall :: Behaviour Picture
flashingBall
    = let ball = shape (ell 0.2 0.2)
      in reg (timeTrans (8 * time) flash)
             (translate (sin time, cos time) ball)
main6 :: IO ()
main6 = animateB "Flashing Ball" flashingBall

revolvingBalls :: Behaviour Picture
revolvingBalls
    = overMany [timeTrans (lift0 (t * pi/4) + time) flashingBall | t <- [0..7]]

main7 :: IO ()
main7 = animateB "Lots of Flashing Balls" revolvingBalls

class Turnable a where
    turn :: Float -> a -> a

instance Turnable Picture where
    turn theta (Region c r) = Region c (turn theta r)
    turn theta (p1 `Over` p2) = turn theta p1 `Over` turn theta p2
    turn theta EmptyPic = EmptyPic

instance Turnable a => Turnable (Behaviour a ) where
    turn theta (Beh b) = Beh (turn theta . b)

rotate :: Float -> Coordinate -> Coordinate
rotate theta (x, y) = (x * c + y * s, y * c - x * s)
                       where (s,c) = (sin theta, cos theta)

instance Turnable Shape where
    turn theta (Polygon ps) = Polygon (map (rotate theta) ps)

instance Turnable Region where
    turn theta (Shape sh) = Shape (turn theta sh)

main8 :: IO ()
main8 = do animateB "kaleido1 (close window for next demo)" kaleido2
           animateB "kaleido2" kaleido1

slowTime = 0.1 * time
kaleido :: Integer -> (Float -> Behaviour Coordinate) -> Behaviour Picture
kaleido n f = lift2 turn (pi * sin slowTime) $
              overMany (zipWith reg (map lift0 (cycle spectrum))
                                    (map (flip turn poly) rads))
              
              where
                rads = map (((2 * pi/fromInteger n)*) . fromInteger) [0..n - 1]
                poly = polyShapeAnim (map f rads)

kaleido1 = kaleido 6 star
    where star x = syncPair (2 * cos (v * c + l), 2 * abs (sin(slowTime * s - l)))
            where v = lift0 x
                  l = v * (slowTime + 1)
                  (s, c) = (sin l, cos l)

kaleido2 = kaleido 9 star
    where star x = syncPair (2 * abs (sin (v * a + slowTime)), 2 * abs (cos (a + slowTime)))
            where v = lift0 x
                  a = v + slowTime * sin (v * slowTime)

syncList :: [Behaviour a] -> Behaviour [a]
syncList l = Beh (\t -> map (\(Beh f) -> f t) l)

syncPair :: (Behaviour a, Behaviour b) -> Behaviour (a,b)
syncPair (Beh x, Beh y) = Beh (\t -> (x t, y t))

-- Create an animated polygon from a list of point behaviours
polyShapeAnim :: [Behaviour Coordinate] -> Behaviour Region
polyShapeAnim = lift1 (Shape . Polygon) . syncList

-- the interesting colors (assuming Black background)
spectrum :: [Color]
spectrum = [c | c <- [minBound..], c /= Black]
