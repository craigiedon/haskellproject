module CustomReplace (sequentialReplace, replaceNthOccurence, hasNthOccurence) where
import Text.Regex
import Data.List
import Debug.Trace
import ParseUtils
import Text.Parsec

sequentialReplace :: Regex -> String -> String -> Int -> String
sequentialReplace pattern originalText replacement lineNum = concat $ sequentialIntersperse lineNum 0 replacement (splitRegex pattern originalText)

sequentialIntersperse :: Int -> Int -> String -> [String] -> [String]
sequentialIntersperse _ _ _ [] = []
sequentialIntersperse _ _ _ [x] = [x]
sequentialIntersperse lineNum seqNum replacement (phrase:line) = phrase : (replacement ++ " (" ++ (show lineNum) ++ "," ++ (show seqNum) ++ ") $ Shape") : sequentialIntersperse lineNum (seqNum + 1) replacement line

hasNthOccurence :: (Parsec String () Bool) -> String -> Int -> Bool
hasNthOccurence parser originalText n
  | n < 0 = False
  | otherwise = case (hasParsedExpression (intercalate "Shape" $ take 2 $ drop n shapeSplitList) parser) of
    Left _ -> False
    Right _ -> True
  where shapeSplitList = splitRegex (mkRegex "Shape") originalText

replaceNthOccurence :: Regex -> String -> String -> Int -> String
replaceNthOccurence subPattern originalText replacement n
  |n < 0 = originalText
  |beginSplit == "" && endSplit == "" = replacementPart
  |beginSplit == "" = intercalate "Shape" [replacementPart,endSplit]
  |endSplit == "" = intercalate "Shape" [beginSplit,replacementPart]
  |otherwise = intercalate "Shape" [beginSplit, replacementPart, endSplit]
  where beginSplit = intercalate "Shape" $ take n shapeSplitList
        endSplit = intercalate "Shape" $ drop (n + 2) shapeSplitList
        replacementPart = subRegex subPattern (intercalate "Shape" $ take 2 $ drop n shapeSplitList) replacement
        shapeSplitList = splitRegex (mkRegex "Shape") originalText
