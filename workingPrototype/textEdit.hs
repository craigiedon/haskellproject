import Text.Regex
import Graphics.UI.Gtk
import Debug.Trace
import Control.Monad
import Control.Monad.IO.Class
import Control.Concurrent
import Control.Exception
import System.Glib.Signals
import Graphics.UI.Gtk.SourceView
import qualified Graphics.UI.Gtk.Gdk.GC as Col
    
import System.Time
import System.IO
import System.Plugins.Make
import System.Plugins.Load
import System.Eval.Haskell
import System.Directory
import Prelude hiding (catch)
import Control.Exception
import System.IO.Error hiding (catch)
    
import UpdatingCanvas
import Animation
import Picture
import Draw
import CustomReplace
import CustomSOE hiding (Window)
import qualified CustomSOE as SOE (Window)
import System.Environment

main :: IO ()
main = do
    initGUI

    timeoutAddFull (yield >> return True)
                 priorityDefaultIdle 50
    window <- windowNew
    vbox <- vBoxNew False 0

    tbuff <- sourceBufferNew Nothing
    tbox <- sourceViewNewWithBuffer tbuff
    sourceViewSetMarkCategoryBackground tbox "trackShapes" (Just(Col.Color 60000 0 0))
    sourceViewSetAutoIndent tbox True
    sourceViewSetInsertSpacesInsteadOfTabs tbox True
    sourceViewSetShowLineNumbers tbox True
    textViewSetWrapMode tbox WrapWord
    widgetSetSizeRequest tbox 500 500

    programTypeHolder <- newMVar AnimatedPic
    
    -- File loaded depends on input arguments.
    -- If user inputs nothing, presentation file is loaded
    commandLineArgs <- getArgs
    let userSelectedFile = case commandLineArgs of
          [] -> "SimpleGraphics.hs"
          (fName:otherArgs) -> fName

    startingCode <- readFile userSelectedFile
    fileChangedMVar <- newMVar False
    textBufferSetText tbuff startingCode

    scroll <- scrolledWindowNew Nothing Nothing
    containerAdd scroll tbox
    
    errorView <- textViewNew
    textViewSetEditable errorView False
    textViewSetWrapMode errorView WrapWord
    errorBuff <- textViewGetBuffer errorView
    widgetSetSizeRequest errorView 500 100

    picAnimButton <- buttonNewWithLabel "Switch to Static Image"
    picAnimButton `on` buttonActivated $ do
      programType <- takeMVar programTypeHolder
      takeMVar fileChangedMVar
      srcString <- extractSource tbox
      writeFile "Test.hs" (modifySource srcString)
      case programType of
        SinglePic -> liftIO $ do 
          buttonSetLabel picAnimButton "Switch to Static Image"
          putMVar programTypeHolder AnimatedPic
        AnimatedPic -> liftIO $ do 
          buttonSetLabel picAnimButton "Switch to Animated Image"
          putMVar programTypeHolder SinglePic
      putMVar fileChangedMVar True
      return ()

    boxPackStart vbox scroll PackNatural 0
    boxPackStart vbox errorView PackNatural 5
    boxPackStart vbox picAnimButton PackNatural 0
    set window [containerBorderWidth := 10,
                windowTitle := "Text editor",
                containerChild := vbox]
    onDestroy window mainQuit
    widgetShowAll window

    
    srcString <- extractSource tbox
    writeFile "Test.hs" (modifySource srcString)
    make "Test.hs" []
    loadStat <- getModuleAnim
    let (oldMod, anim) = case loadStat of
          Right (oldMod, animat) -> (oldMod, animat)
    
    updImgVar <- newMVar (DynamicImage anim)

    runGraphics $ do
         on tbuff bufferChanged (saveText window oldMod tbox fileChangedMVar)
         forkIO(drawLoop updImgVar tbox)
         return ()
    forkIO (compileText oldMod tbox updImgVar fileChangedMVar programTypeHolder errorBuff)

                 
    mainGUI
    
    

saveText :: Window -> Module -> SourceView -> MVar Bool -> IO ()
saveText window oldMod tview fileChangedVar = do
    focussed <- windowIsActive window
    case focussed of
        True -> do
                takeMVar fileChangedVar
                srcString <- extractSource tview
                writeFile "Test.hs" (modifySource srcString)
                putMVar fileChangedVar True
        False -> return ()

compileText :: Module -> SourceView -> MVar UpdatableImage -> MVar Bool -> MVar ProgramType -> TextBuffer -> IO ()
compileText oldMod tview updImgVar fileChangedVar programTypeVar errorBuff = do
    do
        fileChanged <- takeMVar fileChangedVar
        programType <- takeMVar programTypeVar
        case (fileChanged) of
            True -> do
                removeIfExists "Test.o"
                mkStat <- make "Test.hs" []
                case mkStat of
                    MakeSuccess cd fp -> do 
                        textBufferSetText errorBuff fp
                        unload oldMod
                        case programType of
                          SinglePic -> do loadResult <- getModulePic
                                          case loadResult of
                                              Right (md, pic) -> do 
                                                oldImageVar <- takeMVar updImgVar
                                                putMVar updImgVar (StaticImage pic)
                                              Left errors -> textBufferSetText errorBuff (concat errors)
                          AnimatedPic -> do loadResult <- getModuleAnim
                                            case loadResult of
                                                Right (md, anim) -> do
                                                  oldImageVar <- takeMVar updImgVar
                                                  putMVar updImgVar (DynamicImage anim)
                                                Left errors -> textBufferSetText errorBuff (concat errors)
                    MakeFailure (errs) ->  textBufferSetText errorBuff (concat errs)
            False -> do 
              return()
        putMVar fileChangedVar False
        putMVar programTypeVar programType
        threadDelay 300000
        compileText oldMod tview updImgVar fileChangedVar programTypeVar errorBuff
                              
removeIfExists :: FilePath -> IO ()
removeIfExists fileName = removeFile fileName `catch` handleExists
  where handleExists e
          | isDoesNotExistError e = return ()
          | otherwise = throwIO e

extractSource :: SourceView -> IO String
extractSource tview = do
    txtBuff <- textViewGetBuffer tview
    startIt <- textBufferGetStartIter txtBuff
    endIt <- textBufferGetEndIter txtBuff
    srcString <- textBufferGetText txtBuff startIt endIt False
    return srcString

modifySource :: String -> String
modifySource oldSource = unlines $ stickInLineNumbers 1 (lines oldSource) "Shape"

stickInLineNumbers :: Int -> [String] -> String -> [String]
stickInLineNumbers _ [] _ = []
stickInLineNumbers n (currentLine : otherLines) matchString = --(currentLine : otherLines)
  (sequentialReplace (mkRegex matchString) currentLine  "TrackedShape" n) : stickInLineNumbers (n + 1) otherLines matchString

getModulePic :: IO (Either [String] (Module, Picture))
getModulePic = do 
               mv <- load "Test.o" ["."] [] "pic"
               case mv of
                    LoadFailure messages -> return (Left messages)
                    LoadSuccess x y -> return (Right (x, y))

getModuleAnim :: IO (Either [String] (Module, Animation Picture))
getModuleAnim = do
  mv <- load "Test.o" ["."] [] "anim"
  case mv of
    LoadFailure messages -> return (Left messages)
    LoadSuccess x y -> return (Right (x,y))

jumpOut :: AsyncException -> IO ()
jumpOut e = do
                print ("This is the exception you got" ++ show e)
                return ()

data ProgramType = SinglePic | AnimatedPic
