module Picture (Picture (Region, Over, EmptyPic),
                 Color (..),
                 regionToGRegion, shapeToGRegion, xUnion, draw,
                 drawRegionInWindow, drawPic,spaceClose, maybeClear, xWin2, yWin2, pictToList, listToPict,
                 module Region
                ) where
import Draw
import Control.Monad
import CustomReplace
import Text.Regex
import Control.Concurrent
import qualified Graphics.UI.Gtk as GT
import qualified Graphics.UI.Gtk.SourceView as GT
import Region
import CustomSOE hiding (Region)
import qualified CustomSOE as G (Region)

data Picture = Region Color Region
             | Picture `Over` Picture
             | EmptyPic
     deriving Show

drawPic                 :: Window -> Picture -> IO ()
drawPic w (Region c r)   = drawRegionInWindow w c r
drawPic w (p1 `Over` p2) = do drawPic w p2; drawPic w p1
drawPic w EmptyPic       = return ()

drawRegionInWindow :: Window -> Color -> Region -> IO ()
drawRegionInWindow w c r 
  = drawInWindow w 
      (withColor c 
         (drawRegion (regionToGRegion r)))

regionToGRegion :: Region -> G.Region
regionToGRegion r = regToGReg (0,0) (1,1) 0 r

regToGReg :: Vector -> Vector -> Float -> Region -> G.Region
type Vector = (Float,Float)

-- Added in instance of function for Tracked Shape, hopefully works
regToGReg loc sca rot (TrackedShape _ r)
  = regToGReg loc sca rot r

regToGReg loc sca rot (Shape s) 
  = shapeToGRegion loc sca rot s

regToGReg loc (sx,sy) rot (Scale (u,v) r)
  = regToGReg loc (sx*u,sy*v) rot r

regToGReg (lx,ly) (sx,sy) rot (Translate (u,v) r) 
  = regToGReg (lx + u, ly + v) (sx,sy) rot r 

regToGReg (lx, ly) (sx,sy) rot (Rotate rad r)
  = regToGReg (lx,ly) (sx,sy) (rot + rad) r

regToGReg loc sca rot Empty
  = createRectangle (0,0) (0,0)

regToGReg loc sca rot (r1 `Union` r2)
  = primGReg loc sca rot r1 r2 orRegion

regToGReg loc sca rot (r1 `Intersect` r2)
  = primGReg loc sca rot r1 r2 andRegion

regToGReg loc sca rot (r1 `Xor` r2)
  = primGReg loc sca rot r1 r2 xorRegion

regToGReg loc sca rot (Complement  r)
  = primGReg loc sca rot winRect r diffRegion

primGReg :: Vector -> Vector -> Float -> Region -> Region -> (G.Region -> G.Region -> G.Region) -> G.Region
primGReg loc sca rot r1 r2 op
  = let gr1 = regToGReg loc sca rot r1
        gr2 = regToGReg loc sca rot r2
    in  op gr1 gr2

winRect :: Region
winRect = Shape (Rectangle 
                  (pixelToInch xWin) (pixelToInch yWin))

xWin2 = xWin `div` 2 
yWin2 = yWin `div` 2 

shapeToGRegion :: Vector -> Vector -> Float -> Shape -> G.Region

shapeToGRegion (lx,ly) (sx,sy) rot s
  = case s of
      Rectangle s1 s2  
        -> createRotatedRectangle (trans (-s1/2,-s2/2)) 
                                  (trans (s1/2,s2/2))
                                  rot
      Ellipse r1 r2    
        -> createRotatedEllipse (trans (-r1,-r2))
                                (trans ( r1, r2))
                                rot
      Polygon vs      
        -> createPolygon (map (trans.rotatePointCW rot) vs)
      RtTriangle s1 s2 
        -> createPolygon (map (trans.rotatePointCW rot) [(0,0),(s1,0),(0,s2)])
    where trans :: Vertex -> Point
          trans (x,y) = ( xWin2 + inchToPixel (lx+x*sx), 
                          yWin2 - inchToPixel (ly+y*sy) )
                        
rotatePointCW :: Float -> Vector -> Vector
rotatePointCW angle (x,y) = ((cos.negate $ angle) * x - (sin.negate $ angle) * y, (sin.negate $ angle) * x + (cos.negate $ angle) * y)


draw :: String -> Picture -> IO ()
draw s p
  = runGraphics $
    do w <- openWindow s (xWin,yWin)
       drawPic w p
       spaceClose w

xUnion :: Region -> Region -> Region
p1 `xUnion` p2 = (p1 `Intersect` Complement p2) `Union`
                 (p2 `Intersect` Complement p1)

pictToList :: Picture -> [(Color,Region)]
pictToList  EmptyPic      = []
pictToList (Region c r)   = [(c,r)]
pictToList (p1 `Over` p2) = pictToList p1 ++ pictToList p2

listToPict :: [(Color, Region)] -> Picture
listToPict [] = EmptyPic
listToPict [(c,r)] = (Region c r)
listToPict ((c,r):ps) = (Region c r) `Over` listToPict ps

adjust :: [(Color,Region)] -> Coordinate -> 
          (Maybe (Color,Region), [(Color,Region)])

adjust regs p
  = case (break (\(_,r) -> r `containsR` p) regs) of
      (top,hit:rest) -> (Just hit, top++rest)
      (_,[])         -> (Nothing, regs)


loop :: Window -> [(Color,Region)] -> IO ()

loop w regs = 
    do clearWindow w
       sequence_ [ drawRegionInWindow w c r | (c,r) <- reverse regs ]
       (x,y) <- getLBP w
       case (adjust regs (pixelToInch (x - xWin2), 
                          pixelToInch (yWin2 - y) )) of
         (Nothing,  _      ) -> closeWindow w
         (Just hit, newRegs) -> loop w (hit : newRegs)

maybeClear :: Bool -> Window -> IO()
maybeClear b w
   =   if b
       then do clearWindow w
       else return ()


