import Picture
import Animation
import Data.Fixed


--Tree
foliageReg,trunkReg,rootLeftReg,rootRightReg :: Region
foliageReg = Shape $ Ellipse 1.2 0.6
trunkReg = Translate (0,-1) $ Shape $ Rectangle 0.8 2
rootLeftReg = Translate (0.2,-2) $ Shape $ RtTriangle 1 1
rootRightReg = Translate (-0.2,-2) $ Scale (-1,1) $ Shape $ RtTriangle 1 1

--Background
skyReg, sunReg, groundReg :: Region
skyReg = Shape $ Rectangle 6 6
sunReg = Translate (0,-1) $ Shape $ Ellipse 4 4
groundReg = Translate (0,-2.2) $ Shape $ Rectangle 6 1

foliage,trunk,roots,sky,sun,ground :: Picture

foliage = Region Black foliageReg
trunk = Region Black trunkReg
roots = Region Black rootLeftReg `Over` Region Black rootRightReg
sky = Region Blue skyReg
sun = Region White sunReg
ground = Region Black groundReg

pic :: Picture
pic = foldl Over EmptyPic [foliage, ground, roots, trunk, sun, sky]

main :: IO ()
main = draw "Tree" pic
