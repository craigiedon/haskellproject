module Test where
    import Picture
    import Animation

    sunPos, earthPos, moonPos :: Float -> (Float, Float)
    
    sunPos t = (0, 0)
    
    earthPos t = (x,y)
        where 
              x = (fst $ sunPos t) + (1.8 * sin t)
              y =  (snd $ sunPos t) + (1.8 * cos t)
              
    moonPos t = (x,y)
        where
                x = (fst $ earthPos t) + (0.75 * sin (3 * t))
                y = (snd $ earthPos t) + (0.75 * cos (3 * t))
    
    
    sun, earth, moon :: Animation Picture
    sun t = Region Yellow $ Translate (sunPos t )(Shape $ Ellipse 0.8 0.8)
    earth t = Region Blue $ Translate (earthPos t) (Shape $ Ellipse 0.4 0.4)
    moon t = Region White $ Translate (moonPos t) (Shape $ Ellipse 0.2 0.2)
    
    solarSystem :: Animation Picture
    solarSystem t =  moon t `Over` earth t `Over` sun t

    anim :: Animation Picture
    anim = solarSystem