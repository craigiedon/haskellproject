module Test where
    import Picture
    import Animation
    import Data.Fixed

    r1, r2, r3, r4 :: Region
    r1 = (Rotate 0.5 $ Shape(Rectangle 2 2)) `Xor` (Shape (Ellipse 0.2 0.6))
    r2 = Shape(Ellipse 2 1.5)
    r3 = Shape(RtTriangle 3 2)
    r4 = Shape(Polygon [(-2.5, 2.5), (-3.0,0), (-1.7,-1.0), (-1.1,0.2),(-1.5,2.0)])
    r5 = Translate (-1.5, 0) $ Shape(Rectangle 1 0.3)
    r6 = Translate (1.5, 0) $ Shape(Rectangle 1 0.3)

    p1,p2,p3,p4,p5 :: Picture
    p1 = Region Red r1
    p2 = Region Green r2
    p3 = Region Blue r3
    p4 = Region Yellow r4
    p5 = (Region Red r5) `Over` (Region Red r6)

    pic :: Picture
    pic = foldl Over EmptyPic [p1,p2,p3,p4]

    a1, a2 :: Animation Picture 
    a1 t
      | (t `mod'` 2) > 1 = Region Red (Rotate t $ Shape $ Rectangle 1 1)
      | otherwise = Region Blue (Rotate t $ Shape $ Rectangle 1 1)
                    
    a2 t = Region Green (Translate ((cos t), 0) $ Shape (Ellipse 0.2 0.2)) `Over` p5
           
    anim :: Animation Picture
    anim = a1

    makeGreenSquare :: Float -> Picture
    makeGreenSquare n = Region Green $ (Shape (Rectangle n n))
