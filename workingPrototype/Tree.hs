module Test where
import Picture
import Animation
import Data.Fixed

foliageReg, trunkReg, rootLeftReg, rootRightReg, skyReg, sunReg, groundReg :: Region

foliageReg  = Translate (0.110, 0.210) $ Shape (Ellipse 1.2 0.6)
trunkReg = Translate (0.130, -1.020) $ Shape (Rectangle 0.75 1.5)
rootLeftReg = Translate (0.490, -1.770) $ Shape (RtTriangle 0.5 0.5)
rootRightReg = Translate (-0.24, -1.770) $ Shape (RtTriangle (-0.5) 0.5)
skyReg = Translate (0.000, 0.000) $ Shape (Rectangle 6 6)
sunReg = Translate (2.110, 1.580) $ Shape (Ellipse 1 1)
groundReg = Translate (0, -2.75) $ Shape(Rectangle 6 2)

foliage, trunk, roots, sky, sun, ground :: Picture

foliage = Region Green foliageReg
trunk = Region Brown trunkReg
roots = (Region Brown rootLeftReg) `Over` (Region Brown rootRightReg)
sky = Region Cyan skyReg
sun = Region Yellow sunReg
ground = Region Magenta groundReg

pic :: Picture
pic = foldl Over EmptyPic [foliage, trunk, roots,ground,sun,sky]

anim :: Animation Picture
anim t = EmptyPic