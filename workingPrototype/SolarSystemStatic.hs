import Picture
import Animation

sunPos,earthPos,moonPos :: Float -> (Float,Float)
earthPos t = (0,0)
sunPos t = (x,y)
  where
    x = 2 * ((fst $ earthPos t) + cos t)
    y = 2 * ((snd $ earthPos t) + sin t)
moonPos t = (x,y)
  where
    x = (fst $ earthPos t) + cos (4 * t)
    y = (snd $ earthPos t) + sin (4 * t)

sun, earth, moon :: Animation Picture
sun t = Region Yellow $ Translate (sunPos t) $ Shape $ Ellipse 0.8 0.8
earth t = Region Blue $ Translate (earthPos t) $ Shape $ Ellipse 0.4 0.4
moon t = Region White $ Translate (moonPos t) $ Shape $ Ellipse 0.2 0.2

solarSystem :: Animation Picture
solarSystem t = foldl Over EmptyPic [moon t, earth t, sun t]
                               
main :: IO ()
main = animate "Solar System" (picToGraphic . solarSystem)
