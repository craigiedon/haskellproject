import Picture
import Animation
import Data.Fixed

greenBallPos :: Float -> (Float, Float)
greenBallPos t = (x, y)
  where 
        x = 2.5 * cos t
        y = sin (2 * t)

greenBall :: Animation Picture
greenBall t = Region Green $ Translate (greenBallPos t) $ Shape (Ellipse 0.2 0.2)

redBlocks :: Picture
redBlocks = (Region Red leftBlock) `Over` (Region Red rightBlock)
  where leftBlock = Translate (-1.5, 0) $ Shape (Rectangle 1 0.3)
        rightBlock = Translate (1.5, 0) $ Shape (Rectangle 1 0.3)

figureEight :: Animation Picture
figureEight t = greenBall t `Over` redBlocks

main :: IO ()
main = animate "Figure 8" (picToGraphic . figureEight)
