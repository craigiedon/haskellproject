module Test where
    import Picture
    import Animation

    sunPos, earthPos, moonPos :: Float -> (Float, Float)
    
    sunPos t = (0, 0)
    
    earthPos t = (x,y)
        where 
              x = (fst $ sunPos t) + (1.8 * sin t)
              y =  (snd $ sunPos t) + (1.8 * cos t)
              
    moonPos t = (x,y)
        where
                x = 0.4 * (fst $ earthPos t)
                y = 0.4 * (snd $ earthPos t)
    
    
    sun, earth, moon :: Animation Picture
    sun t = Region Yellow $ Translate (earthPos t )(TrackedShape (21,0) $ Shape $ Ellipse 0.8 0.8)
    earth t = Region Blue $ Translate (sunPos t) (TrackedShape (22,0) $ Shape $ Ellipse 0.4 0.4)
    moon t = Region White $ Translate (moonPos t) (TrackedShape (23,0) $ Shape $ Ellipse 0.2 0.2)
    
    solarSystem :: Animation Picture
    solarSystem t =  moon t `Over` earth t `Over` sun t

    anim :: Animation Picture
    anim = solarSystem
