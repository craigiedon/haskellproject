module Region ( Region (Shape, Translate, Scale, Rotate, Complement, TrackedShape,
                        Union, Intersect, Xor, Empty),
                Coordinate,
                containsS, containsR, getRegionCentre, testCentre,
                module Shape
              ) where
import Shape
import Debug.Trace
infixr 5 `Union`
infixr 6 `Intersect`

-- A Region is either:
data Region = Shape Shape		-- primitive shape
            | Translate Vector Region -- translated region
            | Scale Vector Region -- scaled region
            | Rotate Float Region -- rotate in radians
            | Complement Region	-- inverse of region
            | Region `Union` Region   -- union of regions
            | Region `Intersect` Region -- intersection of regions
            | Region `Xor` Region -- XOR of regions
            | Empty                   -- empty region
            | TrackedShape (Int,Int) Region -- Tracks line number and order of occurrence within each line (e.g (5, 2) means fifth line, second occurence)
     deriving Show

type Vector = (Float,Float)

r1 `difference` r2 = r1 `Intersect` (Complement r2)

type Coordinate = (Float,Float)

isLeftOf :: Coordinate -> Ray -> Bool
(px,py) `isLeftOf` ((ax,ay),(bx,by))
       = let (s,t) = (px-ax, py-ay)
             (u,v) = (px-bx, py-by)
         in  s*v >= t*u

type Ray = (Coordinate, Coordinate)

containsR :: Region -> Coordinate -> Bool

pushDownScalesR :: Vector -> Region -> Region
pushDownScalesR (sX, sY) (Shape s) = Shape (scaleShape (sX, sY) s) 
pushDownScalesR (sX, sY) (Scale (u, v) r) = pushDownScalesR (sX * u, sY * v) r
pushDownScalesR scVec (TrackedShape ns r) = TrackedShape ns (pushDownScalesR scVec r)
pushDownScalesR scVec (Translate tVec r) = Translate tVec $ pushDownScalesR scVec r
pushDownScalesR scVec (Rotate rot r) = Rotate rot $ pushDownScalesR scVec r
pushDownScalesR scVec Empty = Empty
pushDownScalesR scVec (r1 `Union` r2) = (pushDownScalesR scVec r1) `Union` (pushDownScalesR scVec r2)
pushDownScalesR scVec (r1 `Intersect` r2) = (pushDownScalesR scVec r1) `Intersect` (pushDownScalesR scVec r2)
pushDownScalesR scVec (r1 `Xor` r2) = (pushDownScalesR scVec r1) `Xor` (pushDownScalesR scVec r2)
pushDownScalesR scVec (Complement r) = Complement (pushDownScalesR scVec r)

scaleShape :: Vector -> Shape -> Shape
scaleShape (sx, sy) s =
    case s of
        Rectangle s1 s2 -> Rectangle (s1 * sx) (s2 * sy)
        Ellipse r1 r2 -> Ellipse (r1 * sx) (r2 * sy)
        Polygon vs -> Polygon [(x * sx, y * sy) | (x,y) <- vs]
                      where (cx, cy) = getShapeCentre s
        RtTriangle s1 s2 -> RtTriangle (s1 * sx) (s2 * sy)
        
getShapeCentre :: Shape -> (Float, Float)
getShapeCentre (Rectangle s1 s2) = (0,0)
getShapeCentre (Ellipse r1 r2) = (0,0)
getShapeCentre (Polygon vs) = let xSum = sum $ map fst vs
                                  ySum = sum $ map snd vs
                                  n = length vs
                              in (xSum / (fromIntegral n), ySum / (fromIntegral n))
getShapeCentre (RtTriangle s1 s2) = (0,0)

getRegionCentre :: Region -> (Float, Float)
getRegionCentre (Shape s) = getShapeCentre s
getRegionCentre (TrackedShape _ r) = getRegionCentre r
getRegionCentre (Translate (x,y) r) = (a + x, b + y)
  where (a,b) = getRegionCentre r
getRegionCentre (Scale (x,y) r) = getRegionCentre r
getRegionCentre (Rotate deg r) = getRegionCentre r
getRegionCentre (r1 `Union` r2) = ((cx1 + cx2) / 2 , (cy1 + cy2) / 2)
  where (cx1, cy1) = getRegionCentre r1
        (cx2, cy2) = getRegionCentre r2
getRegionCentre (r1 `Intersect` r2) = ((cx1 + cx2) / 2 , (cy1 + cy2) / 2)
  where (cx1, cy1) = getRegionCentre r1
        (cx2, cy2) = getRegionCentre r2
getRegionCentre (r1 `Xor` r2) = ((cx1 + cx2) / 2 , (cy1 + cy2) / 2)
  where (cx1, cy1) = getRegionCentre r1
        (cx2, cy2) = getRegionCentre r2
getRegionCentre (Complement r) = getRegionCentre r

testCentre :: Region -> Region
testCentre r = Translate (x,y) $ Shape (Ellipse 0.1 0.1)
  where (x,y) = getRegionCentre r




(TrackedShape _ r) `containsR` p
  = r `containsR` p
(Shape s) `containsR` p
   = s `containsS` p

--Rotation rules
(Rotate ang (Translate (u,v) r)) `containsR` p
   = (Translate (u,v) $ Rotate ang r) `containsR` p
(Rotate ang (Scale (u,v) r )) `containsR` p
   = (Scale (u,v) (Rotate ang r)) `containsR` p
(Rotate rad1 (Rotate rad2 r)) `containsR` p
    = Rotate (rad1 + rad2) r `containsR` p
(Complement (Rotate rad r)) `containsR` p 
   = not ((Rotate rad r) `containsR` p)
(Rotate rad (r1 `Union` r2)) `containsR` p
   = (Rotate rad r1) `containsR` p || (Rotate rad r2) `containsR` p
(Rotate rad (r1 `Intersect` r2)) `containsR` p
   = (Rotate rad r1) `containsR` p && (Rotate rad r2) `containsR` p
(Rotate rad (r1 `Xor` r2)) `containsR` p
   = let a = (Rotate rad r1) `containsR` p
         b = (Rotate rad r2) `containsR` p
     in (a || b) && not (a && b)
(Rotate rad r) `containsR` (x,y)
    = let p = ((cos rad) * x - (sin rad) * y, (sin rad) * x + (cos rad) * y) in r `containsR` p

(Translate (u,v) r) `containsR` (x,y)
   = let p = (x-u,y-v) in r `containsR` p
(Scale (u,v) r) `containsR` (x,y)
   = (pushDownScalesR (u, v) r) `containsR` (x,y)
(Complement r) `containsR` p 
   = not (r `containsR` p)
(r1 `Union` r2)     `containsR` p
   = r1 `containsR` p || r2 `containsR` p
(r1 `Intersect` r2) `containsR` p
   = r1 `containsR` p && r2 `containsR` p
(r1 `Xor` r2) `containsR` p
   = let a = r1 `containsR` p
         b = r2 `containsR` p
     in (a || b) && not (a && b)
Empty `containsR` p 
   = False

containsS :: Shape -> Coordinate -> Bool

(Rectangle s1 s2) `containsS` (x,y)
   = let t1 = abs s1/2; t2 = abs s2/2
     in (-t1<=x) && (x<=t1) && (-t2<=y) && (y<=t2)
(Ellipse r1 r2) `containsS` (x,y)
   = (x/r1)^2 + (y/r2)^2 <= 1
(Polygon pts) `containsS` p
   = let leftOfList = map (isLeftOf p) 
                          (zip pts (tail pts ++ [head pts]))
     in and leftOfList
(RtTriangle s1 s2) `containsS` p
   = if ((s1 >= 0 && s2 >= 0) || (s1 < 0 && s2 < 0)) then
       (Polygon [(0,0),(s1,0),(0,s2)]) `containsS` p
     else
       (Polygon [(0,0), (0,s2), (s1,0)]) `containsS` p
        
univ = Complement Empty
