module ParseUtils where

import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Debug.Trace

getTranslationValues :: String -> Maybe (Float, Float)
getTranslationValues st = Nothing

hasParsedExpression :: String -> (Parsec String () Bool) -> Either ParseError Bool
hasParsedExpression input parser = parse parser "unknown" input

transformationParser :: String -> Parsec String () Bool
transformationParser transOp = do
  manyTill anyChar (try $ string transOp)
  try(transformAndShape) <|> (transformationParser transOp)
  return True
  
rotationParser :: Parsec String () Bool
rotationParser = do
  manyTill anyChar (try $ string "Rotate")
  try(rotateAndShape) <|> rotationParser
  return True
  
transformAndShape = do
  spaces
  char '('
  spaces
  optional $ char '-'
  floats
  spaces
  char ','
  spaces
  optional $ char '-'
  floats
  spaces
  char ')'
  spaces
  char '$'
  spaces
  string "Shape"
  many anyToken
  return True

rotateAndShape = do
  spaces
  try (bracketedFloat) <|> normalFloat
  spaces
  char '$'
  spaces
  string "Shape"
  many anyToken
  return True
  
normalFloat = do
  optional $ char '-'
  floats
  return True

bracketedFloat = do
  char '('
  spaces
  normalFloat
  spaces
  char ')'
  return True
  

lexer = P.makeTokenParser haskellDef
floats = P.naturalOrFloat lexer
