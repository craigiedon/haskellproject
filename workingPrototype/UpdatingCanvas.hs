module UpdatingCanvas(drawLoop) where

import Picture
import GHC.Float
import Animation
import Draw
import Control.Monad
import CustomReplace
import Text.Regex
import Text.Printf
import Control.Concurrent
import qualified Graphics.UI.Gtk as GT
import qualified Graphics.UI.Gtk.SourceView as GT
import Region
import CustomSOE hiding (Region)
import qualified CustomSOE as G (Region)
import Debug.Trace
import ParseUtils

data UpdatingData = UpdatingData {firstRecordedTime :: Word32,
                                  codeView :: GT.SourceView}
                    
data Transformation = Translation | Rotation | Scaling deriving Show

drawLoop :: MVar UpdatableImage -> GT.SourceView -> IO()
drawLoop updImgVar srcView  = do
  w <- openWindowEx "Reload Tester" (Just (0,0)) (Just (xWin, yWin)) drawBufferedGraphic (Just 30)
  t0 <- timeGetTime
  let stateData = UpdatingData{firstRecordedTime = t0, codeView = srcView}
  updImg <- takeMVar updImgVar
  putMVar updImgVar updImg
  case updImg of
    StaticImage stImg -> waitForClickImage w stImg updImgVar Translation stateData
    DynamicImage dyImg -> do
      startTime <- timeGetTime
      waitForClickAnim w dyImg updImgVar 0 startTime stateData
    
waitForClickAnim :: Window -> Animation Picture -> MVar UpdatableImage -> Float -> Word32 -> UpdatingData -> IO ()
waitForClickAnim w anim updImgVar currentAnimTime lastRecTime stateData =
  do t <- timeGetTime
     let dt = intToFloat (word32ToInt (t - lastRecTime)) / 1000
     setGraphic w $ ((picToGraphic).anim) $ currentAnimTime
     drawInWindow w (text (0,0) $ "Animation")
     event <- maybeGetWindowEvent w
     
     updImg <- takeMVar updImgVar
     putMVar updImgVar updImg
     case updImg of
       StaticImage stImg -> waitForClickImage w stImg updImgVar Translation stateData
       DynamicImage newAnim -> case event of
         Just (Key 'p' True) -> pausedAnim w newAnim updImgVar currentAnimTime stateData
         Just (MouseMove (x,y)) -> do
           let regs = pictToList (anim currentAnimTime)
           let aux(_,r) = r `containsR` (pixelToInch (x - xWin2),
                                         pixelToInch (yWin2 - y))
           case (break aux regs) of
             (_,[]) -> do
               txtBuff <- GT.textViewGetBuffer (codeView stateData)
               let srcBuff = GT.castToSourceBuffer txtBuff
               startIt <- GT.textBufferGetStartIter srcBuff
               endIt <- GT.textBufferGetEndIter srcBuff
               GT.sourceBufferRemoveSourceMarks srcBuff startIt endIt "trackShapes"
             (top, (col, hitR):bot) -> highlightRelevantLines (trackRegion hitR) (codeView stateData)
           waitForClickAnim w newAnim updImgVar (currentAnimTime + dt) t stateData
         Just (Button (x,y) True True) -> scrubAnim w newAnim updImgVar currentAnimTime stateData x x False
         _ -> waitForClickAnim w newAnim updImgVar (currentAnimTime + dt) t stateData

pausedAnim :: Window -> Animation Picture -> MVar UpdatableImage -> Float -> UpdatingData -> IO ()
pausedAnim w anim updImgVar currentAnimTime stateData =
  do 
     let offsetTimes = [0.5, 1 .. 3] 
         onionSkinnedGraphic = foldr overGraphic (picToGraphic $ anim currentAnimTime) 
                               [picToTransparentGraphic (float2Double $ 0.5 * (1 - (abs offset) / (1 + maximum offsetTimes))) (anim $ currentAnimTime + offset) | offset <- offsetTimes]

     setGraphic w onionSkinnedGraphic
     drawInWindow w (text (0,0) "Animation : Paused")
     event <- maybeGetWindowEvent w
     updImg <- takeMVar updImgVar
     putMVar updImgVar updImg
     case updImg of
       StaticImage stImg -> waitForClickImage w stImg updImgVar Translation stateData
       DynamicImage newAnim -> case event of
         Just (Key 'p' True) -> do
           restartTime <- timeGetTime
           waitForClickAnim w newAnim updImgVar currentAnimTime restartTime stateData
         Just (MouseMove (x,y)) -> do
           let regs = pictToList (anim currentAnimTime)
           let aux(_,r) = r `containsR` (pixelToInch (x - xWin2),
                                         pixelToInch (yWin2 - y))
           case (break aux regs) of
             (_,[]) -> do
               txtBuff <- GT.textViewGetBuffer (codeView stateData)
               let srcBuff = GT.castToSourceBuffer txtBuff
               startIt <- GT.textBufferGetStartIter srcBuff
               endIt <- GT.textBufferGetEndIter srcBuff
               GT.sourceBufferRemoveSourceMarks srcBuff startIt endIt "trackShapes"
             (top, (col, hitR):bot) -> highlightRelevantLines (trackRegion hitR) (codeView stateData)
           pausedAnim w newAnim updImgVar currentAnimTime stateData
         Just (Button (x,y) True True) -> scrubAnim w newAnim updImgVar currentAnimTime stateData x x True
         _ -> pausedAnim w newAnim updImgVar currentAnimTime stateData

scrubAnim :: Window -> Animation Picture -> MVar UpdatableImage -> Float -> UpdatingData -> Int -> Int -> Bool -> IO ()
scrubAnim w anim updImgVar currentAnimTime stateData origX newX isPaused = do
  setGraphic w $ picToGraphic $ anim currentAnimTime
  drawInWindow w (text (0,0) $ "Animation : Scrubbing")
  let scrubbedTime = currentAnimTime + (fromIntegral $ newX - origX) / 100
  lastRecTime <- timeGetTime
  event <- maybeGetWindowEvent w
  updImg <- takeMVar updImgVar
  putMVar updImgVar updImg
  case updImg of
    StaticImage stImg -> waitForClickImage w stImg updImgVar Translation stateData
    DynamicImage newAnim -> case event of
      Just (MouseMove (x,y)) -> do
        scrubAnim w newAnim updImgVar scrubbedTime stateData newX x isPaused
      Just (Button (x,y) True False) -> do
        if isPaused then pausedAnim w newAnim updImgVar scrubbedTime stateData
          else waitForClickAnim w newAnim updImgVar scrubbedTime lastRecTime stateData
      _ -> scrubAnim w newAnim updImgVar scrubbedTime stateData newX newX isPaused

waitForClickImage :: Window -> Picture -> MVar UpdatableImage -> Transformation ->  UpdatingData -> IO ()
waitForClickImage  w pic updImgVar transOp stateData =
  do
     setGraphic w (picToGraphic pic)
     drawInWindow w (text (0,0) $ "Tool: " ++ (show transOp))
     event <- maybeGetWindowEvent w

     updImg <- takeMVar updImgVar
     putMVar updImgVar updImg
     
     case updImg of
       StaticImage stIm -> do
         let regs = pictToList stIm
         case event of
           Just (Button (x,y) True True) ->
             let aux (_,r) = r `containsR` ( pixelToInch (x - xWin2),
                                             pixelToInch (yWin2 - y))
             in case (break aux regs) of
               (_,[]) -> waitForClickImage w stIm updImgVar transOp stateData
               (top, hit:bot) -> do
                 takeMVar updImgVar
                 putMVar updImgVar (StaticImage (listToPict (hit : (top++bot))))
                 waitForRelease (x,y) (x,y) w updImgVar transOp stateData
                 
           Just (Key 's' True) -> do
                                    print "s was pressed"
                                    waitForClickImage w stIm updImgVar Scaling stateData
           Just (Key 't' True) -> do
                                    print "t was pressed"
                                    waitForClickImage w stIm updImgVar Translation stateData
           Just (Key 'r' True) -> do
                                    print "r was pressed"
                                    waitForClickImage w stIm updImgVar Rotation stateData
           Just (MouseMove (x,y)) -> do
                 let aux (_,r) = r `containsR` (pixelToInch (x - xWin2),
                                                pixelToInch (yWin2 - y))
                 case (break aux regs) of
                   (_,[]) -> do --Just get rid of all currently set source marks
                        txtBuff <- GT.textViewGetBuffer (codeView stateData)
                        let srcBuff = GT.castToSourceBuffer txtBuff
                        startIt <- GT.textBufferGetStartIter srcBuff
                        endIt <- GT.textBufferGetEndIter srcBuff
                        GT.sourceBufferRemoveSourceMarks srcBuff startIt endIt "trackShapes"
                   (top, (col, hitR):bot) -> highlightRelevantLines (trackRegion hitR) (codeView stateData)
                  
                 waitForClickImage w stIm updImgVar transOp stateData
           _ -> waitForClickImage w stIm updImgVar transOp stateData
       DynamicImage dynImg -> do
         startTime <- timeGetTime
         waitForClickAnim w dynImg updImgVar 0 startTime stateData
         

           
waitForRelease :: Point -> Point -> Window -> MVar UpdatableImage -> Transformation -> UpdatingData -> IO ()
waitForRelease (origX, origY) (x,y) w updImgVar transOp stateData
  = do updImg <- takeMVar updImgVar
       let pic = getPic updImg
           regs = pictToList pic
           (c,r) = head regs
           (centreX, centreY) = getRegionCentre r

       setGraphic w (picToGraphic pic)
       drawInWindow w (text (0,0) $ "Tool: " ++ (show transOp))
       originalCode <- getFullText (codeView stateData)
       newHeadReg <- case transOp of
         Translation -> translateTrackedShape (originalCode) (codeView stateData) (pixelToInch (x - origX)) (pixelToInch (origY - y)) r
         Scaling -> do
           let (scaleFactorX, scaleFactorY) = computeScaleFactor (origX, origY) (x,y) (inchToPixel centreX + xWin2, yWin2 - inchToPixel centreY)
           scaleTrackedShape originalCode (codeView stateData) scaleFactorX scaleFactorY r
         Rotation -> do
           let rotationAmount = computeRotation (origX, origY) (x,y) r
           rotateTrackedShape originalCode (codeView stateData) rotationAmount r
       let newPic = listToPict ((c, newHeadReg) : (tail regs))
       putMVar updImgVar (StaticImage $ newPic)
       
       event <- maybeGetWindowEvent w
       case event of
         Just (Button (x,y) True False) -> waitForClickImage w newPic updImgVar transOp stateData
         Just (MouseMove pt) -> waitForRelease (x,y) pt w updImgVar transOp stateData
         _ -> waitForRelease (x,y) (x,y) w updImgVar transOp stateData
       
       --let centrePoints = map (\(c,r) -> Region Red (testCentre r)) regs
       --sequence (map (\r -> drawInWindow w $ picToGraphic r) centrePoints)
               
translateTrackedShape :: [String] -> GT.SourceView -> Float -> Float -> Region -> IO(Region)
translateTrackedShape originalSrc sView x y reg = do 
    txtBuff <- GT.textViewGetBuffer sView
    let srcBuff = GT.castToSourceBuffer txtBuff
    startIt <- GT.textBufferGetStartIter srcBuff
    endIt <- GT.textBufferGetEndIter srcBuff
    fullString <- GT.textBufferGetText srcBuff startIt endIt True
    let stringLines = lines fullString
    case reg of
        (Shape s) -> return (Translate (x,y) (Shape s))
        (Translate (xOld, yOld) (Shape s)) -> return (Translate(x + xOld, y + yOld) (Shape s))
        (Translate (xOld, yOld) (TrackedShape (lineNum, seqNum) s)) -> do
            let lineToAlter = stringLines !! (lineNum - 1)
            let regularNumbersMatch = hasNthOccurence (transformationParser "Translate") lineToAlter seqNum
            let alteredLine = case regularNumbersMatch of
                                   True -> replaceNthOccurence (mkRegex "Translate \\([^,]*,[^,]*\\) \\$ Shape") 
                                           lineToAlter 
                                           ("Translate (" ++ printf "%.3f" (x + xOld) ++ ", " ++ printf "%.3f" (y + yOld)  ++  ") $ " ++ "Shape")
                                           seqNum
                                   False -> replaceNthOccurence (mkRegex "Shape")
                                            lineToAlter
                                            ("Translate (" ++ show (x) ++ ", " ++ show(y) ++ ") $ " ++ "Shape")
                                            seqNum
            txtBuff <- GT.textViewGetBuffer sView
            let srcBuff = GT.castToSourceBuffer txtBuff
            GT.textBufferSetText srcBuff $ unlines $ (take (lineNum -1) stringLines) ++ (alteredLine:(drop lineNum  stringLines))
            return (Translate(x + xOld, y + yOld) (TrackedShape (lineNum, seqNum) s))
        (TrackedShape (lineNum, seqNum) reg) -> do
            let lineToAlter = stringLines !! (lineNum - 1)
            let alteredLine = replaceNthOccurence (mkRegex "Shape") lineToAlter ("Translate (" ++ printf "%.3f" x ++ ", " ++ printf "%.3f" y ++ ") $ " ++ "Shape") seqNum
            txtBuff <- GT.textViewGetBuffer sView
            let srcBuff = GT.castToSourceBuffer txtBuff
            GT.textBufferSetText srcBuff $ unlines $ (take (lineNum - 1) stringLines) ++ (alteredLine:(drop lineNum  stringLines))
            return (Translate (x,y) (TrackedShape (lineNum, seqNum) reg))
        (Translate vec reg) -> do
            modifiedRegion <- translateTrackedShape originalSrc sView x y reg
            return (Translate vec modifiedRegion)
        (Scale vec reg) -> do
            modifiedRegion <- translateTrackedShape originalSrc sView x y reg
            return (Scale vec modifiedRegion)
        (Rotate rad reg) -> do
            modifiedRegion <- translateTrackedShape originalSrc sView x y reg
            return (Rotate rad modifiedRegion)
        (Complement reg) -> do
            modifiedRegion <- translateTrackedShape originalSrc sView x y reg
            return (Complement modifiedRegion)
        (reg1 `Union` reg2) -> do
            modifiedRegion1 <- translateTrackedShape originalSrc sView x y reg1
            modifiedRegion2 <- translateTrackedShape originalSrc sView x y reg2
            return(modifiedRegion1 `Union` modifiedRegion2)
        (reg1 `Intersect` reg2) -> do
            modifiedRegion1 <- translateTrackedShape originalSrc sView x y reg1
            modifiedRegion2 <- translateTrackedShape originalSrc sView x y reg2
            return(modifiedRegion1 `Intersect` modifiedRegion2)
        (reg1 `Xor` reg2) -> do
            modifiedRegion1 <- translateTrackedShape originalSrc sView x y reg1
            modifiedRegion2 <- translateTrackedShape originalSrc sView x y reg2
            return(modifiedRegion1 `Xor` modifiedRegion2)
        Empty -> return Empty


scaleTrackedShape :: [String] -> GT.SourceView -> Float -> Float -> Region -> IO(Region)
scaleTrackedShape  originalSrc sView x y reg = do 
    txtBuff <- GT.textViewGetBuffer sView
    let srcBuff = GT.castToSourceBuffer txtBuff
    startIt <- GT.textBufferGetStartIter srcBuff
    endIt <- GT.textBufferGetEndIter srcBuff
    fullString <- GT.textBufferGetText srcBuff startIt endIt True
    let stringLines = lines fullString
    case reg of
        (Shape s) -> return (Translate (x,y) (Shape s))
        (Scale (xOld, yOld) (Shape s)) -> return (Scale (x + xOld, y + yOld) (Shape s))
        (Scale (xOld, yOld) (TrackedShape (lineNum, seqNum) s)) -> do
            let lineToAlter = stringLines !! (lineNum - 1)
            let regularNumbersMatch = hasNthOccurence (transformationParser "Scale") lineToAlter seqNum
            let alteredLine = case regularNumbersMatch of
                                   True -> replaceNthOccurence (mkRegex "Scale \\([^,]*,[^,]*\\) \\$ Shape") 
                                           lineToAlter 
                                           ("Scale (" ++ printf "%.3f" (x + xOld) ++ ", " ++ printf "%.3f" (y + yOld)  ++  ") $ " ++ "Shape")
                                           seqNum
                                   False -> replaceNthOccurence (mkRegex "Shape")
                                            lineToAlter
                                            ("Scale (" ++ show (x) ++ ", " ++ show(y) ++ ") $ " ++ "Shape")
                                            seqNum
            txtBuff <- GT.textViewGetBuffer sView
            let srcBuff = GT.castToSourceBuffer txtBuff
            GT.textBufferSetText srcBuff $ unlines $ (take (lineNum -1) stringLines) ++ (alteredLine:(drop lineNum  stringLines))
            return (Scale(x + xOld, y + yOld) (TrackedShape (lineNum, seqNum) s))
        (TrackedShape (lineNum, seqNum) reg) -> do
            let lineToAlter = stringLines !! (lineNum - 1)
            let alteredLine = replaceNthOccurence (mkRegex "Shape") lineToAlter ("Scale (" ++ printf "%.3f" (1 + x) ++ ", " ++ printf "%.3f" (1 + y) ++ ") $ " ++ "Shape") seqNum
            txtBuff <- GT.textViewGetBuffer sView
            let srcBuff = GT.castToSourceBuffer txtBuff
            GT.textBufferSetText srcBuff $ unlines $ (take (lineNum - 1) stringLines) ++ (alteredLine:(drop lineNum  stringLines))
            return (Scale (1 + x,1 + y) (TrackedShape (lineNum, seqNum) reg))
        (Translate vec reg) -> do
            modifiedRegion <- scaleTrackedShape originalSrc sView x y reg
            return (Translate vec modifiedRegion)
        (Scale vec reg) -> do
            modifiedRegion <- scaleTrackedShape originalSrc sView x y reg
            return (Scale vec modifiedRegion)
        (Rotate rad reg) -> do
            modifiedRegion <- scaleTrackedShape originalSrc sView x y reg
            return (Rotate rad modifiedRegion)
        (Complement reg) -> do
            modifiedRegion <- scaleTrackedShape originalSrc sView x y reg
            return (Complement modifiedRegion)
        (reg1 `Union` reg2) -> do
            modifiedRegion1 <- scaleTrackedShape originalSrc sView x y reg1
            modifiedRegion2 <- scaleTrackedShape originalSrc sView x y reg2
            return(modifiedRegion1 `Union` modifiedRegion2)
        (reg1 `Intersect` reg2) -> do
            modifiedRegion1 <- scaleTrackedShape originalSrc sView x y reg1
            modifiedRegion2 <- scaleTrackedShape originalSrc sView x y reg2
            return(modifiedRegion1 `Intersect` modifiedRegion2)
        (reg1 `Xor` reg2) -> do
            modifiedRegion1 <- scaleTrackedShape originalSrc sView x y reg1
            modifiedRegion2 <- scaleTrackedShape originalSrc sView x y reg2
            return(modifiedRegion1 `Xor` modifiedRegion2)
        Empty -> return Empty
        
rotateTrackedShape :: [String] -> GT.SourceView -> Float -> Region -> IO(Region)
rotateTrackedShape originalSrc sView angle reg = do
    txtBuff <- GT.textViewGetBuffer sView
    let srcBuff = GT.castToSourceBuffer txtBuff
    startIt <- GT.textBufferGetStartIter srcBuff
    endIt <- GT.textBufferGetEndIter srcBuff
    fullString <- GT.textBufferGetText srcBuff startIt endIt True
    let stringLines = lines fullString
    case reg of
        (Shape s) -> return (Rotate angle (Shape s))
        (Rotate rot (Shape s)) -> return (Rotate (rot + angle) (Shape s))
        (Rotate rot (TrackedShape (lineNum, seqNum) s)) -> do
            let lineToAlter = stringLines !! (lineNum - 1)
            let regularNumbersMatch = hasNthOccurence (rotationParser) lineToAlter seqNum
            let alteredLine = case regularNumbersMatch of
                                   True -> replaceNthOccurence (mkRegex "Rotate .* \\$ Shape") 
                                           lineToAlter 
                                           ("Rotate (" ++ printf "%.3f" (rot + angle) ++ ") $ " ++ "Shape")
                                           seqNum
                                   False -> trace "This is a falsehood" $ replaceNthOccurence (mkRegex "Shape")
                                            lineToAlter
                                            ("Rotate (" ++ printf "%.3f" (angle) ++ ") $ " ++ "Shape")
                                            seqNum
            txtBuff <- GT.textViewGetBuffer sView
            let srcBuff = GT.castToSourceBuffer txtBuff
            GT.textBufferSetText srcBuff $ unlines $ (take (lineNum -1) stringLines) ++ (alteredLine:(drop lineNum  stringLines))
            return (Rotate (rot + angle) (TrackedShape (lineNum, seqNum) s))
        (TrackedShape (lineNum, seqNum) reg) -> do
            let lineToAlter = stringLines !! (lineNum - 1)
            let alteredLine = replaceNthOccurence (mkRegex "Shape") lineToAlter ("Rotate (" ++ printf "%.3f" (angle) ++ ") $ " ++ "Shape") seqNum
            txtBuff <- GT.textViewGetBuffer sView
            let srcBuff = GT.castToSourceBuffer txtBuff
            GT.textBufferSetText srcBuff $ unlines $ (take (lineNum - 1) stringLines) ++ (alteredLine:(drop lineNum  stringLines))
            return (Rotate angle (TrackedShape (lineNum, seqNum) reg))
        (Translate vec reg) -> do
            modifiedRegion <- rotateTrackedShape originalSrc sView angle reg
            return (Translate vec modifiedRegion)
        (Scale vec reg) -> do
            modifiedRegion <- rotateTrackedShape originalSrc sView angle reg
            return (Scale vec modifiedRegion)
        (Rotate rad reg) -> do
            modifiedRegion <- rotateTrackedShape originalSrc sView angle reg
            return (Rotate rad modifiedRegion)
        (Complement reg) -> do
            modifiedRegion <- rotateTrackedShape originalSrc sView angle reg
            return (Complement modifiedRegion)
        (reg1 `Union` reg2) -> do
            modifiedRegion1 <- rotateTrackedShape originalSrc sView angle reg1
            modifiedRegion2 <- rotateTrackedShape originalSrc sView angle reg2
            return(modifiedRegion1 `Union` modifiedRegion2)
        (reg1 `Intersect` reg2) -> do
            modifiedRegion1 <- rotateTrackedShape originalSrc sView angle reg1
            modifiedRegion2 <- rotateTrackedShape originalSrc sView angle reg2
            return(modifiedRegion1 `Intersect` modifiedRegion2)
        (reg1 `Xor` reg2) -> do
            modifiedRegion1 <- rotateTrackedShape originalSrc sView angle reg1
            modifiedRegion2 <- rotateTrackedShape originalSrc sView angle reg2
            return(modifiedRegion1 `Xor` modifiedRegion2)
        Empty -> return Empty
  

computeScaleFactor :: Point -> Point -> Point -> (Float, Float)
computeScaleFactor (origX, origY) (newX, newY) (cx, cy) =
  let origDistX = sqrt . fromIntegral $ (origX - cx) ^ 2
      origDistY = sqrt . fromIntegral $ (origY - cy) ^ 2
      newDistX = sqrt . fromIntegral $ (newX - cx) ^ 2
      newDistY = sqrt . fromIntegral $ (newY - cy) ^ 2
      
  in ((newDistX - origDistX) / intToFloat xWin2 , (newDistY - origDistY) / intToFloat yWin2)
     
computeRotation :: Point -> Point -> Region -> Float
computeRotation (origX, origY) (newX, newY) r =
  let (cX,cY) = screenToWorld.getRegionCentre $ r
      (origVX, origVY) = (fromIntegral $ origX - cX, fromIntegral $ origY - cY)
      (newVX, newVY) = (fromIntegral $ newX - cX, fromIntegral $ newY - cY)
  in (atan2 newVY newVX) - (atan2 origVY origVX)
     
screenToWorld :: (Float, Float) -> Point
screenToWorld (centreX, centreY) = (inchToPixel centreX + xWin2, yWin2 - inchToPixel centreY)

trackRegion :: Region -> [Int] -- Takes a region and tries to see if there is a tracked shape nested somewhere within it
trackRegion Empty = []
trackRegion (Shape _) = []
trackRegion (TrackedShape (lineNum, seqNum) _) = [lineNum]
trackRegion (Scale _ r) = trackRegion r
trackRegion (Rotate _ r) = trackRegion r
trackRegion (Translate _ r) = trackRegion r
trackRegion (Complement r) = trackRegion r
trackRegion (r1 `Union` r2) = (trackRegion r1) ++ (trackRegion r2)
trackRegion (r1 `Xor` r2) = (trackRegion r1) ++ (trackRegion r2)
trackRegion (r1 `Intersect` r2) = (trackRegion r1) ++ (trackRegion r2)

highlightRelevantLines :: [Int] -> GT.SourceView -> IO ()
highlightRelevantLines lineNums srcView = do
    txtBuff <- GT.textViewGetBuffer srcView
    let srcBuff = GT.castToSourceBuffer txtBuff
    startIt <- GT.textBufferGetStartIter srcBuff
    endIt <- GT.textBufferGetEndIter srcBuff
    GT.sourceBufferRemoveSourceMarks srcBuff startIt endIt "trackShapes"

    highlightLine lineNums srcBuff

highlightLine :: [Int] -> GT.SourceBuffer -> IO ()
highlightLine [] _ = return ()
highlightLine (lineNum:lineNums) srcBuff = do
  currentLineIter <- GT.textBufferGetIterAtLine srcBuff (lineNum - 1)
  GT.sourceBufferCreateSourceMark srcBuff Nothing "trackShapes" currentLineIter
  highlightLine lineNums srcBuff

getFullText :: GT.SourceView -> IO[String]
getFullText srcView = do
    txtBuff <- GT.textViewGetBuffer srcView
    let srcBuff = GT.castToSourceBuffer txtBuff
    startIt <- GT.textBufferGetStartIter srcBuff
    endIt <- GT.textBufferGetEndIter srcBuff
    fullString <- GT.textBufferGetText srcBuff startIt endIt True
    return (lines fullString)