#Programming as An Interactive Experience Source Files

This repository contains the source files for my Final Year University Project "Programming as an Interactive Experience".

(From the abstract)

_This project explores the feasibility of creating programming tools in
Haskell which update their result immediately when the user makes a
change. Such tools would allow programmers to alter a program��s source
code and instantly perceive the effects of their changes without saving,
compiling, and running the program themselves. Such a system could also
provide two-way interaction by letting the programmer manipulate the result
of a running program. Such manipulations should lead automatically
and instantly to the corresponding changes in the source code.
The main contribution of this project was the development of a programming
tool for simple 2D graphics and animation which embodies the
above principles. The tool was written in Haskell, for Haskell programs.
Implementing the system in a purely functional language provides the opportunity
to use techniques such as higher-order functions. Additionally
properties of the language, such as the absence of side-effects, simplifies
particular problems while making others more challenging. The project
also includes a 12 person user evaluation of the above tool to assess the
usefulness of such interactive programming tools._

You can read the full dissertation in the file _finalReport.pdf_. The source is contained in the _workingPrototype_ folder

##Prequisites

In order to run this project you will need the haskell platform install on your machine, and likely the following packages. The best way to install most of them is by using the [Haskell Cabal](https://www.haskell.org/cabal/) package manager:

* [System.Plugins](http://hackage.haskell.org/package/plugins-1.5.1.4)
* [Gtk2HS](https://hackage.haskell.org/package/gtk)
* [GtkSourceView](https://wiki.gnome.org/Projects/GtkSourceView)
* [SOEGtk](https://hackage.haskell.org/package/soegtk)

##Running the Editor

To compile the editor, run

**ghc --make textEdit.hs**

To run the editor, run

**./textEdit <filename>**

For example, to run the SolarSystem example, type:

**./textEdit SolarSystem.hs**
